@testable import ChuckNorrisJokes

extension JokeViewCellModel: Equatable {
    
    public static func == (lhs: JokeViewCellModel, rhs: JokeViewCellModel) -> Bool {
        lhs.jokeText == rhs.jokeText && lhs.id == rhs.id
    }
    
    convenience init(jokeText: String, id: Int) {
        self.init(joke: .init(text: jokeText, id: id))
    }
    
}
