import XCTest
import Combine

extension XCTestCase {
    
    func awaitPublisher<T: Publisher>(_ publisher: T) -> T.Output {
        
        var result: T.Output!
        let expectation = expectation(description: "Awaiting publisher")

        let cancellable = publisher.sink(receiveCompletion: { _ in expectation.fulfill() },
                                         receiveValue: { value in result = value })

        waitForExpectations(timeout: 1)
        cancellable.cancel()
        
        return result
    }
    
}
