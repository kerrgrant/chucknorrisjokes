import Combine

extension Published.Publisher {
    
    func collectNext(_ count: Int) -> AnyPublisher<[Output], Never> {
            collect(count)
            .first()
            .eraseToAnyPublisher()
    }
}
