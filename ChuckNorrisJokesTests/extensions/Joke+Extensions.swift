@testable import ChuckNorrisJokes

extension Joke {
    
    init(text: String, id: Int) {
        self.init(response: .init(joke: text, id: id))
    }
    
}
