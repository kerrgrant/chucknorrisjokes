import XCTest
@testable import ChuckNorrisJokes

class JokeListRepoTests: XCTestCase {
    
    private lazy var mockURLSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockURL.self]
        return URLSession(configuration: configuration)
    }()
    
    private var expectation: XCTestExpectation!
    private lazy var sut: JokeListRepo = {
        .init(urlSession: mockURLSession)
    }()

    override func setUpWithError() throws {
        try super.setUpWithError()
        expectation = expectation(description: "Response expectation")
    }
    
    /*
        Given:
            Initialised sut
     
        When:
            Network responds with status 200 and correct JSON payload
        
        Then:
            Closure called passing a success result with the expected joke list
     */
    func test_1() {
        
        MockURL.requestHandler = { _ in (HTTPURLResponse(statusCode: 200), successJson) }
        
        sut.requestJokes { [weak self] result in
            if case .success(let jokes) = result {
                XCTAssertEqual(jokes, [.init(text: "Chuck Norris once rode a bull, and nine months later it had a calf.", id: 385),
                                       .init(text: "If at first you don't succeed, you're not Chuck Norris.", id: 184),
                                       .init(text: "Chuck Norris invented the internet? just so he had a place to store his porn.", id: 218),
                                       .init(text: "Dark spots on the Moon are the result of Chuck Norris' shooting practice.", id: 589)])
            } else {
                XCTFail("Request failed despite correct json and 200 status code")
            }
            self?.expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    /*
        Given:
            Initialised sut
     
        When:
            Network error is thrown
        
        Then:
            Closure called passing a failure result specifying networkFailure
     */
    func test_2() {
        
        MockURL.requestHandler = { _ in throw MockNetworkError.simulated }
        
        sut.requestJokes { [weak self] result in
            switch result {
            case .success:
                XCTFail("Request succeeded despite network error.")
                
            case .failure(let error):
                XCTAssertEqual(error, .networkFailure)
            }
            self?.expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    /*
        Given:
            Initialised sut
     
        When:
            Network responds with status 400 and no JSON payload
        
        Then:
            Closure called passing a failure result specifying networkFailure
     */
    func test_3() {
        
        MockURL.requestHandler = { _ in (HTTPURLResponse(statusCode: 400), nil) }
        
        sut.requestJokes { [weak self] result in
            
            switch result {
            case .success:
                XCTFail("Request succeeded despite 400 status code.")
                
            case .failure(let error):
                XCTAssertEqual(error, .networkFailure)
            }
            
            self?.expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    /*
        Given:
            Initialised sut
     
        When:
            Network responds with status 200 and JSON payload root containing type != 'success'
        
        Then:
            Closure called passing a failure result specifying decodeFailure
     */
    func test_4() {
        
        MockURL.requestHandler = { _ in (HTTPURLResponse(statusCode: 200), nonSuccessJson) }
        
        sut.requestJokes { [weak self] result in
            switch result {
            case .success:
                XCTFail("Succeeded despite 'type' != 'success'")
                
            case .failure(let error):
                XCTAssertEqual(error, .decodeFailure)
            }
            self?.expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    /*
        Given:
            Initialised sut
     
        When:
            Network responds with status 200 and a badly formatted JSON payload
        
        Then:
            Closure called passing a failure result specifying decodeFailure
     */
    func test_5() {
        
        MockURL.requestHandler = { _ in (HTTPURLResponse(statusCode: 200), illFormattedJson) }
        
        sut.requestJokes { [weak self] result in
            switch result {
            case .success:
                XCTFail("Request succeeded with incorrect json format and shouldn't have.")
                
            case .failure(let error):
                XCTAssertEqual(error, .decodeFailure)
            }
            self?.expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }

}

//MARK: URLProtocol mock
private class MockURL: URLProtocol {
    
    static var requestHandler: ((URLRequest) throws -> (HTTPURLResponse, Data?))?
  
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        // This is where you create the mock response as per your test case and send it to the URLProtocolClient.
        guard let handler = MockURL.requestHandler else {
            fatalError("Handler is unavailable.")
        }
        
        do {
            let (response, data) = try handler(request)
            
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            
            if let data = data {
                client?.urlProtocol(self, didLoad: data)
            }
            
            client?.urlProtocolDidFinishLoading(self)
            
        } catch {
            client?.urlProtocol(self, didFailWithError: error)
        }
    }
    
    override func stopLoading() {
        // This is called if the request gets canceled or completed.
    }
    
}

private let successJson = """
{ "type": "success", "value": [ { "id": 385, "joke": "Chuck Norris once rode a bull, and nine months later it had a calf.", "categories": [] }, { "id": 184, "joke": "If at first you don't succeed, you're not Chuck Norris.", "categories": [] }, { "id": 218, "joke": "Chuck Norris invented the internet? just so he had a place to store his porn.", "categories": [] }, { "id": 589, "joke": "Dark spots on the Moon are the result of Chuck Norris' shooting practice.", "categories": [] } ]  }
""".data(using: .utf8)

private let nonSuccessJson = """
{ "type": "something other than success", "value": [ { "id": 385, "joke": "Chuck Norris once rode a bull, and nine months later it had a calf.", "categories": [] }, { "id": 184, "joke": "If at first you don't succeed, you're not Chuck Norris.", "categories": [] }, { "id": 218, "joke": "Chuck Norris invented the internet? just so he had a place to store his porn.", "categories": [] }, { "id": 589, "joke": "Dark spots on the Moon are the result of Chuck Norris' shooting practice.", "categories": [] } ]  }
""".data(using: .utf8)

private let illFormattedJson = """
{ "type": "something other than success", "categories": [] }, { "id": 589, "joke": "Dark spots on the Moon are the result of Chuck Norris' shooting practice.", "categories": [] } ]  }
""".data(using: .utf8)

private enum MockNetworkError: Error {
    case simulated
}

private extension HTTPURLResponse {
    
    convenience init(statusCode: Int) {
        self.init(url: URL(string: "https://www.google.com")!,
                  statusCode: statusCode,
                  httpVersion: nil,
                  headerFields: nil)!
    }
    
}
