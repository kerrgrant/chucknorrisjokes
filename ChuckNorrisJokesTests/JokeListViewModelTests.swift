import Foundation
import XCTest
import Combine
@testable import ChuckNorrisJokes

class JokeListViewModelTests: XCTestCase {
    
    private var isLoading: [Bool]!
    private var jokes: [[JokeViewCellModel]]!
    private var errors: [ErrorAlertViewModel?]!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        isLoading = []
        jokes = []
        errors = []
    }
    
    /*
        Given:
            Initialised sut with succeeding repo
     
        When:
            Request for jokes ISN'T made
        
        Then:
            'isLoading' should send a single 'false' event, as it isn't loading (yet).
            'jokes' should send a single empty list of cell viewmodels
            'error' should send a single nil event (i.e. no error)
     */
    func test_1() {
        
        let sut = JokeListViewModel(repo: MockJokeListRepo(succeeds: true))
        
        isLoading = awaitPublisher(sut.$isLoading.collectNext(1))
        jokes = awaitPublisher(sut.$jokes.collectNext(1))
        errors = awaitPublisher(sut.$error.collectNext(1))
        
        XCTAssertEqual([false], isLoading)
        XCTAssertEqual([[]], jokes)
        XCTAssertEqual([nil], errors)
    }
    
    /*
        Given:
            Initialised sut with succeeding repo
     
        When:
            A request for jokes IS made
        
        Then:
            'isLoading' should send a sequence of events indicating that it WASN'T initially loading, then was, then WASN'T (as loading completed)
            'jokes' should send a single list of cell viewmodels
            'error' should send a single nil event
     */
    func test_2() {

        let sut = JokeListViewModel(repo: MockJokeListRepo(succeeds: true))
        
        isLoading += awaitPublisher(sut.$isLoading.collectNext(1))
        
        sut.requestJokes()
        
        isLoading += awaitPublisher(sut.$isLoading.collectNext(2))
        jokes += awaitPublisher(sut.$jokes.collectNext(1))
        errors += awaitPublisher(sut.$error.collectNext(1))
        
        XCTAssertEqual([false, true, false], isLoading)
        XCTAssertEqual([[.init(jokeText: "Joke 1", id: 1),
                         .init(jokeText: "Joke 2", id: 2),
                         .init(jokeText: "Joke 3", id: 3)]],
                       jokes)
        XCTAssertEqual([nil], errors)
    }
    
    /*
        Given:
            Initialised sut with failing repo
     
        When:
             No request made for jokes
        
         Then:
             'isLoading' should send a single 'false' event as it isn't loading (yet).
             'jokes' should send a single empty list of cell viewmodels
             'error' should send a single nil event
     */
    func test_3() {
        
        let sut = JokeListViewModel(repo: MockJokeListRepo(succeeds: false))
        
        isLoading = awaitPublisher(sut.$isLoading.collectNext(1))
        jokes = awaitPublisher(sut.$jokes.collectNext(1))
        errors = awaitPublisher(sut.$error.collectNext(1))
        
        XCTAssertEqual([false], isLoading)
        XCTAssertEqual([[]], jokes)
        XCTAssertEqual([nil], errors)
    }
    
    /*
        Given:
            Initialised sut with failing repo
     
        When:
            A request for jokes is made
        
        Then:
            'isLoading' should send a sequence of events indicating that it WASN'T initially loading, then WAS, then WASN'T (as loading completed)
            'jokes' shouldn't send any cell viewmodel events (as repo fails)
            'error' should send a single error event indicating that the request has failed
     */
    func test_4() {
        
        let sut = JokeListViewModel(repo: MockJokeListRepo(succeeds: false))
        
        isLoading += awaitPublisher(sut.$isLoading.collectNext(1))
        
        sut.requestJokes()
        
        isLoading += awaitPublisher(sut.$isLoading.collectNext(2))
        jokes += awaitPublisher(sut.$jokes.collectNext(1))
        errors += awaitPublisher(sut.$error.collectNext(1))
        
        XCTAssertEqual([false, true, false], isLoading)
        XCTAssertEqual([[]], jokes)
        XCTAssertEqual([.init(title: "Lolz over",
                              message: "Chuck Norris has decided that\'s enough fun for today.",
                              buttonText: "Ok")],
                       errors)
    }
    
    /*
        Given:
            Initialised sut

        Then:
            'title' should equal "Chuck Norris lolz"
            'refreshButtonText' should equal "Refresh Jokes"
        
     */
    func test_5() {
        let sut = JokeListViewModel(repo: MockJokeListRepo(succeeds: true))
        XCTAssertEqual(sut.title, "Chuck Norris lolz")
        XCTAssertEqual(sut.refreshButtonText, "Refresh Jokes")
    }
    
}

private struct MockJokeListRepo: JokeListRepoProtocol {
    
    let succeeds: Bool
    
    func requestJokes(completion: @escaping (Result<[Joke], JokeListError>) -> Void) {
        
        if succeeds {
            let jokes: [Joke] = [.init(text: "Joke 1", id: 1),
                                 .init(text: "Joke 2", id: 2),
                                 .init(text: "Joke 3", id: 3)]
            completion(.success(jokes))
        } else {
            completion(.failure(.networkFailure))
        }
    }
    
}
