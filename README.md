# README #

As requested, an iOS application utilising the API [http://www.icndb.com/api/](http://www.icndb.com/api/).

### Specification ###

* iOS 14+
* Swift, SwiftUI, Combine
* MVVM, Repository
* Unit tested
* Basic error handling
* No 3rd party dependencies
* Built using XCode Version 13.1 (13A1030d)

### Improvements ###

* Improve error UI to inform the user when/if they don't have an internet connection.
* Improve UI styling (as it's currently quite crude).
* Reset the scroll position of the list to the top when the jokes are refreshed.
* Add unit tests for ErrorAlertViewModel. 
* Add appropriate app icon.
* Add UI tests using mocked data.
* If networking became more complex (i.e. multiple endpoints) then the modelling and construction of endpoints would be more structured (i.e. baseURL etc). I've kept it simple as currently there is only one.
* If data persistence was required, i'd abstract the networking implementation from the repository into to a remote data source, injecting that into repository. I would then create an abstracted local data source object, and equivalently inject that into the repo, modifying the repo to effectively managed when each data source is called (i.e. cache policy). This would shield the rest of the app from that change.
