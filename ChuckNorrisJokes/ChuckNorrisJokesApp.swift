import SwiftUI

@main
struct ChuckNorrisJokesApp: App {
    var body: some Scene {
        WindowGroup {
            JokeListView(viewModel: .init(repo: JokeListRepo(urlSession: URLSession.shared)))
        }
    }
}
