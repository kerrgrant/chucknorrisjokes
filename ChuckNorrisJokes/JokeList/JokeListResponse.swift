struct JokeListResponse: Decodable {
    let type: ResponseType
    let value: [JokeResponse]
}

extension JokeListResponse {
    
    enum ResponseType: String, Decodable {
        case success
    }
    
}
