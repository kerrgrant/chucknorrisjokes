import Foundation

final class JokeListViewModel: ObservableObject {
    
    private let repo: JokeListRepoProtocol
    
    @Published var isLoading = false
    @Published var jokes: [JokeViewCellModel] = []
    @Published var error: ErrorAlertViewModel?
    
    let refreshButtonText = "Refresh Jokes"
    let title = "Chuck Norris lolz"
    
    init(repo: JokeListRepoProtocol) {
        self.repo = repo
    }
    
    func requestJokes() {
        
        isLoading = true
        error = nil
        
        repo.requestJokes { [weak self] result in
            DispatchQueue.main.async { [weak self] in
                self?.handleResult(result: result)
            }
        }
    }
    
    private func handleResult(result: Result<[Joke], JokeListError>) {
        switch result {
        case .success(let jokes):
            handleSuccess(jokes: jokes)
            
        case .failure(let error):
            handleError(error: error)
        }
        isLoading = false
    }
    
    private func handleSuccess(jokes: [Joke]) {
        self.jokes = jokes.map { .init(joke: $0) }
    }
    
    private func handleError(error: JokeListError) {
        switch error {
        case .networkFailure, .decodeFailure:
            self.error = .init(title: "Lolz over",
                               message: "Chuck Norris has decided that's enough fun for today.",
                               buttonText: "Ok")
        }
    }
    
}
