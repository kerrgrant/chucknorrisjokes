import Foundation

struct ErrorAlertViewModel: Identifiable, Equatable {
    let id: String = "ErrorAlertViewModel.id"
    let title: String
    let message: String
    let buttonText: String
}
