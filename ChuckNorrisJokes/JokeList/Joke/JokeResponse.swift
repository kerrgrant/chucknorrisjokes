//MARK: - Network Response Object
struct JokeResponse: Decodable {
    let joke: String
    let id: Int
}
