import SwiftUI

struct JokeViewCell: View {
    
    private let viewModel: JokeViewCellModel
    
    init(viewModel: JokeViewCellModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        Text("\(viewModel.jokeText)")
            .padding()
    }
}

struct JokeViewCell_Previews: PreviewProvider {
    static var previews: some View {
        JokeViewCell(viewModel: .init(joke: .init(response: .init(joke: "Nothing can escape the gravity of a black hole, except for Chuck Norris. Chuck Norris eats black holes. They taste like chicken.",
                                                                  id: 45))))
            .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
            .previewDisplayName("iPhone")
        
        JokeViewCell(viewModel: .init(joke: .init(response: .init(joke: "Nothing can escape the gravity of a black hole, except for Chuck Norris. Chuck Norris eats black holes. They taste like chicken.",
                                                                  id: 45))))
            .previewDevice(PreviewDevice(rawValue: "iPad Pro (12.9-inch) (5th generation)"))
            .previewDisplayName("iPad ")
    }
}
