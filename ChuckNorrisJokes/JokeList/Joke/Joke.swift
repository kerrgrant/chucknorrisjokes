//MARK: - Domain Object
struct Joke: Equatable {
    let text: String
    let id: Int
    
    init(response: JokeResponse) {
        self.text = response.joke
        self.id = response.id
    }
}
