final class JokeViewCellModel: Identifiable {
    
    private let joke: Joke
    
    init(joke: Joke) {
        self.joke = joke
    }
    
    var jokeText: String {
        return joke.text
    }
    
    var id: Int {
        return joke.id
    }
}
