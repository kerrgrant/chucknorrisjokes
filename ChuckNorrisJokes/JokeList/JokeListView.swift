import SwiftUI

struct JokeListView: View {
    
    @StateObject var viewModel: JokeListViewModel
    
    var body: some View {
        ZStack {
            VStack {
                Text(viewModel.title)
                    .font(Font.system(size:30, design: .default))
                List(viewModel.jokes, id: \.id) {
                    JokeViewCell(viewModel: $0)
                }
                .onAppear {
                    viewModel.requestJokes()
                }
                Button(viewModel.refreshButtonText) {
                    viewModel.requestJokes()
                }
            }
            .padding(16)
            .alert(item: $viewModel.error) { errorViewModel in
                Alert(title: Text(errorViewModel.title),
                      message: Text(errorViewModel.message),
                      dismissButton: .default(Text(viewModel.refreshButtonText),
                                              action: { viewModel.requestJokes() }))
            }
            
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .gray))
                    .scaleEffect(2)
            }
        }
    }
}

struct JokeListView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        JokeListView(viewModel: .init(repo: MockJokeListRepo()))
                    .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
                    .previewDisplayName("iPhone")

        JokeListView(viewModel: .init(repo: MockJokeListRepo()))
                    .previewDevice(PreviewDevice(rawValue: "iPad Pro (12.9-inch) (5th generation)"))
                    .previewDisplayName("iPad ")
        
    }
}

// MARK:- Mocked Repo and support classes
private struct MockJokeListRepo: JokeListRepoProtocol {
    
    func requestJokes(completion: @escaping (Result<[Joke], JokeListError>) -> Void) {
        
        let jokes: [Joke] = [.init(text: "Nothing can escape the gravity of a black hole, except for Chuck Norris. Chuck Norris eats black holes. They taste like chicken.",
                                   id: 1),
                             .init(text: "Only Chuck Norris can prevent forest fires.",
                                   id: 2),
                             .init(text: "Human cloning is outlawed because of Chuck Norris, because then it would be possible for a Chuck Norris roundhouse kick to meet another Chuck Norris roundhouse kick. Physicists theorize that this contact would end the universe.",
                                   id: 3),
                             .init(text: "The original title for Alien vs. Predator was Alien and Predator vs Chuck Norris. The film was cancelled shortly after going into preproduction. No one would pay nine dollars to see a movie fourteen seconds long.",
                                   id: 4),
                             .init(text: "Chuck Norris can solve the Towers of Hanoi in one move.",
                                   id: 5)]
        completion(.success(jokes))
    }
    
}

private extension Joke {
    
    init(text: String, id: Int) {
        self.init(response: .init(joke: text, id: id))
    }
    
}
