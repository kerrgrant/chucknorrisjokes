import Foundation

protocol JokeListRepoProtocol {
    
    func requestJokes(completion: @escaping (Result<[Joke], JokeListError>) -> Void)
    
}

enum JokeListError: Error {
    case networkFailure
    case decodeFailure
}

final class JokeListRepo: JokeListRepoProtocol {
    
    private var task: URLSessionDataTask?
    private let urlSession: URLSession
    
    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    func requestJokes(completion: @escaping (Result<[Joke], JokeListError>) -> Void) {
        
        guard let url = URL(string: "https://api.icndb.com/jokes/random/100/?exclude=[explicit]&escape=javascript") else {
            completion(.failure(.networkFailure))
            return
        }
        
        task?.cancel()
        task = urlSession.dataTask(with: url,
                                   completionHandler: { data, response, error in
            
            if let _ = error {
                completion(.failure(.networkFailure))
                return
            }
            
            guard let response = response as? HTTPURLResponse, 200..<300 ~= response.statusCode else {
                completion(.failure(.networkFailure))
                return
            }
            
            guard let responseData = data else {
                completion(.failure(.decodeFailure))
                return
            }
            
            do {
                let decoded = try JSONDecoder().decode(JokeListResponse.self, from: responseData)
                completion(.success(decoded.value.map { .init(response: $0) }))
            } catch {
                completion(.failure(.decodeFailure))
            }
        })
        task?.resume()
    }
    
}
